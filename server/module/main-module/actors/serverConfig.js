import { ObjectId } from 'promised-mongo';
import db from '../../../db';

const {
  dispatch, spawn, spawnStateless, stop,
} = require('nact');

const initialState = db.client.findOne({ application_type: 'server' });

// {
//
//             client_id: "",
//             client_name: "",
//             client_secret: "",
//             client_secret_expires_at: "",
//             application_type: "",
//             grant_types: "",
//             contacts: "",
//             api_uri: "",
//             uri: "",
//             external_system: "",
//             external_token: ""
//
// }

const serverConfig = (parent) => spawn(
  parent,
  async (state, msg, ctx) => {
    const client = await db.client.findOne({ application_type: 'server' });

    dispatch(ctx.sender, client, ctx.self);
  },
  'server_config',

);
// ,{initialState: initialState}
export default serverConfig;

// throw new AuthorizationError('you must be logged in to query this schema');

// https://www.npmjs.com/package/role-acl
// https://onury.io/accesscontrol/?content=faq
// https://developer.mindsphere.io/concepts/concept-roles-scopes.html
// https://wso2.com/library/articles/2015/12/article-role-based-access-control-for-apis-exposed-via-wso2-api-manager-using-oauth-2.0-scopes/
// https://onury.io/accesscontrol/
