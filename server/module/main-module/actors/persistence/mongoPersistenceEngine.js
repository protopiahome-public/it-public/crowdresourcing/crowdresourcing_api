import { ObjectId } from 'promised-mongo';

const { AbstractPersistenceEngine, PersistedEvent, PersistedSnapshot } = require('nact/lib/persistence');
const assert = require('assert');

// TODO experiment

let mongodb = require('mongodb');

let mongojs = require('mongojs');
const mongoist = require('mongoist');

class MongoPersistenceEngine extends AbstractPersistenceEngine {
  constructor(connectionString) {
    super();
    this.db = (async () => {
      let db = mongoist(mongojs(connectionString, [
        'journal',
        'snapshots',
      ]));
      return db;
    })();
  }

  static mapDbModelToDomainModel(dbEvent) {
    return new PersistedEvent(
      dbEvent.data,
      dbEvent.sequence_nr,
      dbEvent.persistence_key,
      dbEvent.tags,
      dbEvent.created_at,
      dbEvent.is_deleted,
    );
  }

  static mapDbModelToSnapshotDomainModel(dbSnapshot) {
    if (dbSnapshot) {
      return new PersistedSnapshot(
        dbSnapshot.data,
        dbSnapshot.sequence_nr,
        dbSnapshot.persistence_key,
        dbSnapshot.created_at,
      );
    }
  }

  events(persistenceKey, offset = 0, limit = null, tags) {
    assert(typeof (persistenceKey) === 'string');
    assert(Number.isInteger(offset));
    assert(Number.isInteger(limit) || limit === null);
    assert(tags === undefined || (tags instanceof Array && tags.reduce((isStrArr, curr) => isStrArr && typeof (curr) === 'string', true)));

    const args = [persistenceKey, offset, limit, tags].filter((x) => x !== undefined);

    const result = this.db.journal.find({ _id: new ObjectId(user.id) }).then((results) => results.map(MongoPersistenceEngine.mapDbModelToDomainModel));
    return new Result(result);
  }

  async persist(persistedEvent) {
    return this.db.journal.insert({
      key: persistedEvent.key,
      sequenceNumber: persistedEvent.sequenceNumber,
      createdAt: persistedEvent.createdAt,
      data: persistedEvent.data,
      tags: persistedEvent.tags,
    });
  }

  async latestSnapshot(persistenceKey) {
    assert(typeof (persistenceKey) === 'string');

    return await this.db.journal.findOne({
      key: persistedEvent.key,
    }).then(MongoPersistenceEngine.mapDbModelToSnapshotDomainModel);
  }

  async takeSnapshot(persistedSnapshot) {
    return await this.db.snapshots.insert({
      key: persistedSnapshot.key,
      sequenceNumber: persistedSnapshot.sequenceNumber,
      createdAt: persistedSnapshot.createdAt,
      data: persistedSnapshot.data,
    });
  }
}

module.exports.MongoPersistenceEngine = MongoPersistenceEngine;

// https://habr.com/ru/post/300956/
// https://habr.com/ru/post/146429/
// https://habr.com/ru/post/149464/
// https://habr.com/ru/post/258693/
// https://github.com/eventsourcing/es4j-graphql
// https://habr.com/ru/company/naumen/blog/257477/
// https://buildplease.com/pages/fpc-15/
