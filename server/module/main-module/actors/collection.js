import { ObjectId } from 'promised-mongo';
import db from '../../../db';

const { spawnStateless, dispatch } = require('nact');

export default (parent) => spawnStateless(
  parent,
  async (msg, ctx) => {
    const type = msg.type.slice();
    let collection;
    delete (msg.type);

    if (msg.search) {
      if (msg.search._id) {
        msg.search._id = new ObjectId(msg.search._id);
      }

      collection = db.collection(type).aggregate([{ $match: msg.search }, { $sort: { title: 1 } }]);
    } else {
      collection = db.collection(type).aggregate([{ $sort: { title: 1 } }]);
    }
    dispatch(ctx.sender, collection, ctx.self);
  },
  'collection',
);
