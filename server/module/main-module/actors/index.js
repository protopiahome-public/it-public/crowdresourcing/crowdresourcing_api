import collection from './collection';
import collectionItem from './collectionItem';
import configCollection from './configCollection';
import serverConfig from './serverConfig';

// module.exports = {
//
//         collection: collection,
//         collectionItem: collectionItem,
//         configCollection: configCollection,
//
// }

export default function (serverActor) {
  const collectionActor = collection(serverActor);
  const collectionItemActor = collectionItem(serverActor);
  const configCollectionActor = configCollection(serverActor);
  const serverConfigActor = serverConfig(serverActor);
}
