"use strict";
// https://www.apollographql.com/docs/apollo-server/features/errors/

import {ApolloError} from 'apollo-server';

export class InvalidTokenError extends ApolloError {
    constructor(message) {
        super(message, 'INVALID_TOKEN');

        Object.defineProperty(this, 'name', { value: 'InvalidRequestError' });
    }
}

export class ExpiredTokenError extends ApolloError {
    constructor(message: string) {
        super(message, 'EXPIRED_TOKEN');

        Object.defineProperty(this, 'name', { value: 'ExpiredTokenError' });
    }
}



export class UnauthorizedClientError extends ApolloError {
    constructor(message: string) {
        super(message, 'UNAUTHORIZED_CLIENT');

        Object.defineProperty(this, 'name', { value: 'UnauthorizedClientError' });
    }
}

export class AccessDeniedError extends ApolloError {
    constructor(message: string) {
        super(message, 'ACCESS_DENIED');

        Object.defineProperty(this, 'name', { value: 'AccessDeniedError' });
    }
}