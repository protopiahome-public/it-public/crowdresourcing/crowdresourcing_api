export default function (scopes) {
  let claims = [];

  scopes.forEach((item) => {
    switch (item) {
      case 'profile':
        claims.push(['name', 'family_name', 'given_name', 'middle_name', 'nickname', 'preferred_username', 'profile', 'picture', 'website', 'gender', 'birthdate', 'zoneinfo', 'locale', 'updated_at']);
        break;
      case 'email':
        claims.push(['email', 'email_verified']);
        break;
      case 'address':
        claims.push(['address']);
        break;
      case 'phone':
        claims.push(['phone_number', 'phone_number_verified']);
        break;
    }
  });

  return claims;
}
