import AssertionTokenType from '../../../../token-types/AssertionTokenType';
import createClientsKeyStore from '../../../../token-types/createClientsKeyStore';
import { UnauthorizedClientError } from '../../errors';

const { query } = require('nact');

export default async function (assertion, ctx) {
  const collectionItemActor = ctx.children.get('item');
  const collectionActor = ctx.children.get('collection');

  let clients = await query(collectionActor, { type: 'client' }, 2000);

  const keystore = await createClientsKeyStore(clients);

  const token = new AssertionTokenType(assertion, keystore);
  await token.verify();
  const key = await token.getKey();

  const key_json = await key.toJSON(true);
  const client_id = key_json.client_id;
  const client_secret = key_json.k;

  let client = await query(collectionItemActor, { type: 'client', search: { _id: client_id, client_secret } }, 2000);

  if (!client) {
    throw new UnauthorizedClientError('client not authorized');
  }

  return { client, key };
}
