const { query } = require('nact');

export default async function (client, user, scope, ctx) {
  const collectionItemActor = ctx.children.get('item');

  const authenticate_session = await query(collectionItemActor, {
    type: 'authenticate_session',
    input: {
      user_id: user._id,
      client_id: client._id,
      scope,
    },
  }, 2000);

  return authenticate_session._id;
}
