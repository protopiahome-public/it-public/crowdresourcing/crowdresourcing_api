import loginHint from './loginHint';
import loginUserCode from './loginUserCode';
import loginHintToken from './loginHintToken';
import idHintToken from './idHintToken';

module.exports = {

  idHintToken,
  loginHintToken,
  loginUserCode,
  loginHint,

};
