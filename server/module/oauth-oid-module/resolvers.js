import {
  authorizationHandler, tokenHandler, revokeHandler, introspectHandler,
} from './oauth-oid';

const { query } = require('nact');

module.exports = {

  Mutation: {

    authorize: async (obj, args, ctx, info) => authorizationHandler(args.input, ctx),

    token: async (obj, args, ctx, info) => tokenHandler(args.input, ctx),

    revoke: async (obj, args, ctx, info) => revokeHandler(args.input, ctx),
  },
  Query: {

    introspect: async (obj, args, ctx, info) => introspectHandler(args.input, ctx),
  },
};
