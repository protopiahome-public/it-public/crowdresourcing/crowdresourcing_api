import { GraphQLModule } from '@graphql-modules/core';

import { readFileSync } from 'fs';
import gql from 'graphql-tag';
import resolvers from './resolvers';
import MainModule from '../main-module';

export default function (ctx) {
  const schema = readFileSync(`${__dirname}/schema.graphqls`, 'utf8');

  const typeDefs = gql`
        ${schema}
    `;

  return new GraphQLModule({
    name: 'CrowdResourcingModule',
    resolvers,
    typeDefs: schema,
    imports: [MainModule(ctx)],

  });
}
