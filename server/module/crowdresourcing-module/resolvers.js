// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'place';

module.exports = {

  Mutation: {
    changeProject: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      if (args._id) {
        return await query(collectionItemActor, { type: 'project', search: { _id: args._id, owner_id: new ObjectId(context.user._id) }, input: args.input }, 2000);
      }
      args.input.owner_id = new ObjectId(context.user._id);
      args.input.add_date = new Date();
      return await query(collectionItemActor, { type: 'project', input: args.input }, 2000);
    },
    deleteRequest: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      let request = await query(collectionItemActor, { type: 'request', search: { _id: args._id } }, 2000);
      let project = await query(collectionItemActor, { type: 'project', search: { _id: request.project_id, owner_id: new ObjectId(context.user._id) } }, 2000);
      if (!project) {
        return;
      }
      await query(collectionItemActor, { type: 'request', search: { _id: args._id }, delete: true }, 2000);
    },
    unapproveRequestAnswer: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      let request_answer = await query(collectionItemActor, { type: 'request_answer', search: { _id: args.request_answer_id } }, 2000);
      let request = await query(collectionItemActor, { type: 'request', search: { _id: request_answer.request_id } }, 2000);
      let project = await query(collectionItemActor, { type: 'project', search: { _id: request.project_id, owner_id: new ObjectId(context.user._id) } }, 2000);
      if (!project) {
        return;
      }
      await query(collectionItemActor, { type: 'request_answer', search: { _id: args.request_answer_id }, input: { is_approved: false, approve_date: new Date() } }, 2000);
    },
    approveRequestAnswer: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      let request_answer = await query(collectionItemActor, { type: 'request_answer', search: { _id: args.request_answer_id } }, 2000);
      let request = await query(collectionItemActor, { type: 'request', search: { _id: request_answer.request_id } }, 2000);
      let project = await query(collectionItemActor, { type: 'project', search: { _id: request.project_id, owner_id: new ObjectId(context.user._id) } }, 2000);
      if (!project) {
        return;
      }
      await query(collectionItemActor, { type: 'request_answer', search: { _id: args.request_answer_id }, input: { is_approved: true, approve_date: new Date() } }, 2000);
    },
    deleteRequestAnswer: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      await query(collectionItemActor, { type: 'request_answer', search: { _id: args._id, owner_id: new ObjectId(context.user._id) }, delete: true }, 2000);
    },
    deleteProject: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      await query(collectionItemActor, { type: 'project', search: { _id: args._id, owner_id: new ObjectId(context.user._id) }, delete: true }, 2000);
    },
    changePost: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      if (args._id) {
        return await query(collectionItemActor, { type: 'post', search: { _id: args._id, owner_id: new ObjectId(context.user._id) }, input: args.input }, 2000);
      }
      args.input.owner_id = new ObjectId(context.user._id);
      args.input.add_date = new Date();
      args.input.project_id = new ObjectId(args.project_id);
      let project = await query(collectionItemActor, { type: 'project', search: { _id: args.project_id, owner_id: new ObjectId(context.user._id) } }, 2000);
      if (!project) {
        return;
      }
      return await query(collectionItemActor, { type: 'post', input: args.input }, 2000);
    },
    changeRequest: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      if (args._id) {
        let request = await query(collectionItemActor, { type: 'request', search: { _id: args._id } }, 2000);
        let project = await query(collectionItemActor, { type: 'project', search: { _id: request.project_id, owner_id: new ObjectId(context.user._id) } }, 2000);
        if (!project) {
          return;
        }
        return await query(collectionItemActor, { type: 'request', search: { _id: args._id }, input: args.input }, 2000);
      }
      args.input.project_id = new ObjectId(args.project_id);
      args.input.add_date = new Date();
      let project = await query(collectionItemActor, { type: 'project', search: { _id: args.project_id, owner_id: new ObjectId(context.user._id) } }, 2000);
      if (!project) {
        return;
      }
      return await query(collectionItemActor, { type: 'request', input: args.input }, 2000);
    },
    changeRequestAnswer: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      if (args._id) {
        return await query(collectionItemActor, { type: 'request_answer', search: { _id: args._id, owner_id: new ObjectId(context.user._id) }, input: args.input }, 2000);
      }
      args.input.owner_id = new ObjectId(context.user._id);
      args.input.request_id = new ObjectId(args.request_id);
      args.input.add_date = new Date();
      let request = await query(collectionItemActor, { type: 'request', search: { _id: args.request_id } }, 2000);
      if (!request) {
        return;
      }
      return await query(collectionItemActor, { type: 'request_answer', input: args.input }, 2000);
    },
  },

  Query: {
    getRequest: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      return await query(collectionItemActor, { type: 'request', search: { _id: args.id } }, 2000);
    },
    getRequestAnswer: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      return await query(collectionItemActor, { type: 'request_answer', search: { _id: args.id } }, 2000);
    },
    getPost: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      return await query(collectionItemActor, { type: 'post', search: { _id: args.id } }, 2000);
    },
    getProject: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      return await query(collectionItemActor, { type: 'project', search: { _id: args.id } }, 2000);
    },
    getProjects: async (obj, args, context, info) => {
      const collectionActor = context.children.get('collection');
      return await query(collectionActor, { type: 'project' }, 2000);
    },
    getMyProjects: async (obj, args, context, info) => {
      const collectionActor = context.children.get('collection');
      return await query(collectionActor, { type: 'project', search: { owner_id: new ObjectId(ctx.user_id) } }, 2000);
    },
  },

  Project: {
    owner: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      return await query(collectionItemActor, { type: 'user', search: { _id: obj.owner_id } }, 2000);
    },
    requests: async (obj, args, context, info) => {
      const collectionActor = context.children.get('collection');
      obj.requests = await query(collectionActor, { type: 'request', search: { project_id: new ObjectId(obj._id) } }, 2000);
      return obj.requests;
    },
    news: async (obj, args, context, info) => {
      const collectionActor = context.children.get('collection');
      obj.news = await query(collectionActor, { type: 'post', search: { project_id: new ObjectId(obj._id) } }, 2000);
      return obj.news;
    },
  },
  Request: {
    request_answers: async (obj, args, context, info) => {
      const collectionActor = context.children.get('collection');
      obj.request_answers = await query(collectionActor, { type: 'request_answer', search: { request_id: new ObjectId(obj._id) } }, 2000);
      return obj.request_answers;
    },
  },
  RequestAnswer: {
    owner: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      return await query(collectionItemActor, { type: 'user', search: { _id: obj.owner_id } }, 2000);
    },
  },
  Post: {
    owner: async (obj, args, context, info) => {
      const collectionItemActor = context.children.get('item');
      return await query(collectionItemActor, { type: 'user', search: { _id: obj.owner_id } }, 2000);
    },
  },
};
