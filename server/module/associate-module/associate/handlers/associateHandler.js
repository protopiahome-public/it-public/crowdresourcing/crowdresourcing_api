import userCodeType from '../code-types/userCodeType';
import selfChannel from '../oob-channels/selfChannel';
import telegramChannel from '../oob-channels/telegramChannel';
import slackChannel from '../oob-channels/slackChannel';
import vkChannel from '../oob-channels/vkChannel';
import emailChannel from '../oob-channels/emailChannel';
import authenticatorResponseOOB from '../response/authenticatorResponseOOB';
import authenticatorResponseOTP from '../response/authenticatorResponseOTP';
import { InvalidAuthenticatorTypeError } from '../../errors';

// (msg, ctx)
export default async function (args, ctx) {
  // переписать
  const client_id = ctx.client._id;
  const current_user = ctx.user;
  const user_id = current_user._id;

  // client_id+user_id просят по каналу определяемому authenticator_type и oob_channel для associate_hint высылается user_code
  const authenticator_type = args.authenticator_type;
  const oob_channel = args.oob_channel;
  const associate_hint = args.associate_hint;
  const for_channel = args.for_channel;

  const user_code = await userCodeType.generate();

  if (user_code) {
    switch (authenticator_type) {
      case 'otp':
        selfChannel({
          client_id, user_id, user_code, for_channel,
        }, ctx);
        return authenticatorResponseOTP('otp', user_code);
        break;
      case 'oob':
        switch (oob_channel) {
          case 'email':
            emailChannel({
              client_id,
              user_id,
              associate_hint,
              user_code,
            },
            ctx);
            return authenticatorResponseOOB('oob', 'email');
            break;
          case 'telegram':
            telegramChannel(
              {
                client_id,
                user_id,
                associate_hint,
                user_code,
              },
              ctx,
            );
            return authenticatorResponseOOB('oob', 'telegram');
            break;
          case 'slack':
            slackChannel(
              {
                client_id,
                user_id,
                associate_hint,
                user_code,
              },
              ctx,
            );
            return authenticatorResponseOOB('oob', 'slack');
            break;
          case 'vk':
            vkChannel(
              {
                client_id,
                user_id,
                associate_hint,
                user_code,
              },
              ctx,
            );
            return authenticatorResponseOOB('oob', 'vk');
            break;
          default:
            break;
        }

        break;
      case 'recovery-code':
        // recoveryCodeType.generate();
        break;
      default:

        break;
    }
  } else {
    throw new InvalidAuthenticatorTypeError('Invalid Authenticator Type');
  }

  // "auth_req_id"
}
