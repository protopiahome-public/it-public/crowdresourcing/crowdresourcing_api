import fs from 'fs';
import db from '../db';
import AssertionTokenType from '../token-types/AssertionTokenType';
import createClientsKeyStore from '../token-types/createClientsKeyStore';

const path = require('path');

async function main() {
  const clients = await db.client.find();
  const keystore = await createClientsKeyStore(clients);
  console.log(keystore.toJSON(true));

  const file = 'assertion.json';
  const dir = __dirname;
  const assertion_tokens = JSON.parse(fs.readFileSync(path.join(dir, file), 'utf8'));

  for (let assertion_token of assertion_tokens) {
    const assertion = assertion_token.assertion_token;
    const client_secret = assertion_token.client_secret;
    const client_id = assertion_token.client_id;

    const token = new AssertionTokenType(assertion, keystore);
    const t = await token.verify();
    const key_json = t.key.toJSON(true);
    console.log(`kid: ${key_json.kid} ; client_id: ${client_id}`);
  }

  process.exit();
}

main();
