import fs from 'fs';
import AssertionTokenType from '../token-types/AssertionTokenType';
import createClientKey from '../token-types/createClientKey';
import db from '../db';

async function main() {
  let result = [];
  let clients = await db.client.find();
  for (let client of clients) {
    const key = await createClientKey(client);
    const token = new AssertionTokenType('', key);
    await token.createSignToken(client, client);
    let data = {
      client_id: client._id,
      application_type: client.application_type,
      client_secret: client.client_secret,
      assertion_token: token.token,
    };
    result.push(data);
    console.log(`kid: ${client._id} ; k: ${client.client_secret}`);
  }

  fs.writeFileSync(`${__dirname}/assertion.json`, JSON.stringify(result), 'utf8');
  console.log('assertion.json writed');
  process.exit();
}

main();
