// import AssertionTokenType from "../token-types/AssertionTokenType"
import { ObjectId } from 'promised-mongo';
import TokenType from '../token-types/TokenType';
import db from '../db';
import createClientKey from '../token-types/createClientKey';

let jose = require('node-jose');

// const token = new AssertionTokenType();
// const key = TokenType.toKey();
// token.setKey(key);
// console.log(TokenType.asKey(key));
// token.sign();

async function main() {
  let clients = await db.client.find();
  for (let client of clients) {
    const key = await createClientKey(client);
    let key_secret = key.toJSON(true).k;
    await db.client.findAndModify({
      query: { _id: new ObjectId(client._id) },
      update: {
        $set: { client_secret: key_secret },
      },
    });
    console.log(`kid: ${client._id} ; k: ${key_secret}`);
  }
  console.log('secrets updated');
}

main();
