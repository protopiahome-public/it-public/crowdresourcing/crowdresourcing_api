let file_users = cat('users.json');
let file_clients = cat('clients.json');

let users = JSON.parse(file_users);
let clients = JSON.parse(file_clients);

users.forEach((user) => {
  user.register_time = new Date();
  db.user.insertOne(user);
});

clients.forEach((client) => {
  db.client.insertOne(client);
});
