// TODO experiment

let mongodb = require('mongodb');

let mongojs = require('mongojs');
const mongoist = require('mongoist');

/**
 * MongoDB connection resource definition.
 */
class MongoDbConnectionResource {
  /**
     * Resource initialization logic.
     *
     * @param system Actor system instance.
     * @returns {Promise} Initialization promise.
     */
  initialize(system) {
    this.log = system.getLog();
    this.log.info('Initializing MongoDB connection resource...');

    return mongodb.MongoClient.connect('mongodb://localhost:27017/test')
      .then((connection) => {
        this.log.info('MongoDB connection resource successfully initialized.');

        this.connection = connection;
      });
  }

  /**
     * Resource destruction logic.
     *
     * @returns {Promise} Destruction promise.
     */
  destroy() {
    this.log.info('Destroying MongoDB connection resource...');

    return this.connection.close().then(() => {
      this.log.info('MongoDB connection resource successfully destroyed.');
    });
  }
}

module.exports = MongoDbConnectionResource;
