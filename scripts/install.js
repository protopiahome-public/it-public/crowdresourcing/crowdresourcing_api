import {
  installModules, cloneModule, cloneLib, createClients,
  createUsers, createAssertionFile, createClientConfig, acceptInstall,
} from './functions';
import modules from '../server/config/modules.json';
import clients from '../server/config/clients.json';
import users from '../server/config/users.json';

async function main() {
  // cloneLib("https://gitlab.com/protopiahome/pe-modules/token-types", "token-types");

  await acceptInstall();

  console.log('Create clients...');
  await createClients(clients);
  console.log('Create users...');
  await createUsers(users);
  console.log('Create assertions...');
  await createAssertionFile();
  console.log('Create client config...');
  await createClientConfig();
  // await installModules(modules);

  process.exit();
  // process.end();
}

main();
